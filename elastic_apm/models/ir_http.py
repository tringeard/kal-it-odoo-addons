# Copyright 2020 Kal-It (https://kal-it.fr)
# @author Timothée Ringeard <timothee.ringeard@kal-it.fr>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

import os

from odoo.addons.base.models.ir_http import IrHttp
from odoo.http import request

from .apm_client import apm_client, elasticapm, is_true
from .http import get_data_from_request

BLACKLISTED_PATHS = ["/longpolling/"]


# Monkey path the dispatch method to start a transaction
def dispatch_wrapper(odoo_dispatch):
    odoo_dispatch = odoo_dispatch.__func__

    def apm_path_blacklist():
        is_blacklisted = False
        path_info = request.httprequest.environ.get("PATH_INFO")
        if any(path_info.startswith(path) for path in BLACKLISTED_PATHS):
            is_blacklisted = True
        return is_blacklisted

    def _dispatch(*args, **kwargs):
        path_info = request.httprequest.environ.get("PATH_INFO")
        if apm_path_blacklist():
            response = odoo_dispatch(*args, **kwargs)
        else:
            apm_client.begin_transaction("request")
            elasticapm.set_user_context(user_id=request.session.uid)
            response = odoo_dispatch(*args, **kwargs)
            elasticapm.set_context(lambda: get_data_from_request(), "request")
            try:
                http_status = response.status_code
            except Exception:
                try:
                    http_status = response.code
                except Exception:
                    http_status = "HttpStatusNotFound"
            apm_client.end_transaction(path_info, http_status)
        return response

    return classmethod(_dispatch)


if is_true(os.environ.get("ELASTIC_APM")):
    IrHttp._dispatch = dispatch_wrapper(IrHttp._dispatch)
