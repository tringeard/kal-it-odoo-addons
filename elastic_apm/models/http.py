# Copyright 2020 Kal-It (https://kal-it.fr)
# @author Timothée Ringeard <timothee.ringeard@kal-it.fr>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

import os

from odoo.exceptions import (
    AccessDenied,
    AccessError,
    MissingError,
    RedirectWarning,
    UserError,
    ValidationError,
)
from odoo.http import WebRequest, request

from .apm_client import apm_client, elasticapm, is_true

HANDLED_EXCEPTIONS = [
    AccessDenied,
    AccessError,
    MissingError,
    RedirectWarning,
    UserError,
    ValidationError,
]


def get_data_from_request():
    httprequest = request.httprequest

    data = {
        "headers": dict(**httprequest.headers),
        "method": httprequest.method,
        "socket": {
            "remote_address": httprequest.remote_addr,
            "encrypted": httprequest.scheme == "https",
        },
        "url": elasticapm.utils.get_url_dict(httprequest.url),
        # Although Cookies are inside the request headers we need to put
        # them here to abide by the context schema of APM
        "cookies": httprequest.cookies,
    }

    # As we do not need duplicated cookies let's pop one out
    data["headers"].pop("Cookie", None)
    return data


odoo_handle_exception = WebRequest._handle_exception


def _handle_exception(self, exception):
    exc_handled = False
    if any(
        isinstance(exception, exc_class) for exc_class in HANDLED_EXCEPTIONS
    ):
        exc_handled = True
    # import pdb; pdb.set_trace();
    elasticapm.label(
        exc_type=type(exception).__name__, exc_source="http_request",
    )
    context = {"request": get_data_from_request()}

    # If an error occurs before _dispatch() is called there is no transaction,
    # so apm_client.capture_exception() fails with ValueError because no
    # active exception is found. This can happen for a 404 Not Found error
    try:
        apm_client.capture_exception(
            context=context, handled=exc_handled,
        )
    except ValueError:
        param_message = {
            "message": "%s - %s",
            "params": (type(exception).__name__, exception.name),
        }
        # Even if no active exception is found send a message to
        # apm server without transaction, unfortunately the apm python agent
        # does not allow us to attach labels to the message
        apm_client.capture_message(
            param_message=param_message, context=context, handled=exc_handled,
        )
    return odoo_handle_exception(self, exception)


if is_true(os.environ.get("ELASTIC_APM")):
    WebRequest._handle_exception = _handle_exception
