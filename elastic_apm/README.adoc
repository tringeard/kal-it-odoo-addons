= Elastic APM

This addon integrate the https://www.elastic.co/apm[APM] features from the ELK suite.

== Configuration

* `ELASTIC_APM` has to be `1` or `true`
* `ELASTIC_APM_SECRET_TOKEN` is the secret token from APM server
* `ELASTIC_APM_SERVER_URL` is the APM server url

This addon must be added in the server wide addons with the `--load` option: +
`--load=web,elastic_apm`

== Features
